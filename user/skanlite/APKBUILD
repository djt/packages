# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=skanlite
pkgver=2.0.1
pkgrel=0
pkgdesc="Simple image scanning application"
url="https://www.kde.org/applications/graphics/skanlite/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libksane-dev kio-dev
	kcoreaddons-dev ki18n-dev kxmlgui-dev kdoctools-dev libpng-dev zlib-dev
	ktextwidgets-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/skanlite/2.0/skanlite-$pkgver.tar.xz
	fix-version.patch
	review-129989.patch
	png-review-129988.patch"
builddir="$srcdir/skanlite-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2dc29f3aeca32a7afb515ae9349fab7ef635de731718b53ceaf532cbd3860fbb07bd936fd2988b6dddb74109775e22752ffd05a0a3d0218b9c8ae393af52b14e  skanlite-2.0.1.tar.xz
80411112612025cb061960e7baf013efbe36d78a1567d169f313b9d39563c2d816c637f3b0ce45980e4a8ae8bf42e74677e32735fbfc5d8d63b6d637dcfac0fc  fix-version.patch
d20473d75ba9be608fedc843fe6128e4b57a20af0d13dd88c36ab93efc3e11c764431935e3bab2cf495f9735f221236c5c1d061fbc863a26eb3a637be9bd0436  review-129989.patch
a960317f96dad1424cebd6b4734ce98c7f7401385b4434b961a0304283cd1d98bd99788be21eaa9e2bf0a5a01f6c647b6b5709334e0bd3f2b65fc12e3c56cbf3  png-review-129988.patch"
