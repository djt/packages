From e9eaf4c80ef0f90f53ee7aa284e56e808dcbe6c4 Mon Sep 17 00:00:00 2001
From: Alexander Trufanov <trufanovan@gmail.com>
Date: Wed, 8 Mar 2017 17:55:33 +0300
Subject: Bugfix: wrong folder selection dialog behavior

REVIEW:129989

There are 2 problems with directory selection dialog.
1. If you scan very first page you'll get SaveLocation dialog but if you press the "..." button in it to specify folder for scans - a file selection dialog will be opened. Not directory selection, but file selection.  If you select a file in it - a directory selection dialog pop up just after you close previous one. So you'll get a second dialog. Your first file selection result will be overwritten by folder selection.
2. If you choose for example "/tmp/" in directory selection dialog you'll get "/tmp" in the text box. Without trailing path separator. And resulted filename preview will be "/prefix-0001.png", not "/tmp/prefix-0001.png". If you didn't notice that and press ok you'll get an error as nothing can be saved to root. If you selected another folder you later may realize that your scans are in parent directory. Why? Bcs SkanLite doesn't check trailing path separator in directory path and believes that "/tmp" is a file tmp in the root folder. And Qt's directory selection dialogs always return directory name without trailing path separator in it.

I've fixed both problems.
Second one is easy, just `if (!dir.endsWith(QDir::separator())) dir = dir.append(QDir::separator());` before proceeding with directory selection results.

First one is a bit awkward. There is a slot `getDir()` and 2 widgets connected to it. `u_urlRequester` from SaveLocation dialog and `getDirButton` from Settings dialog. the slot opens QFileDialog::getExistingDirectory(). It's fine for getDirButton which is a QButton. But u_urlRequester is a KUrlRequester and contains own button and launches own file selection dialog which is for files by default. That's obviously wrong.
There are notes in  `getDir()` slot about `// FIXME KF5`. Seems to be [this](https://mail.kde.org/pipermail/kde-frameworks-devel/2014-March/013276.html) discussion. I believe it's irrelevant to this problem. Perhaps this problem is a former workaround for KF5 problem which is already disappeared.

All you need to fix this behavior is to disconnect KUrlRequester from getDir() which explicitly calls getExistingDirectory() and let it use it's own. We just need to switch it in directory selection mode with `u_urlRequester->setMode(KFile::Directory);`.
---
 src/SaveLocation.cpp | 5 +++--
 src/skanlite.cpp     | 4 +---
 2 files changed, 4 insertions(+), 5 deletions(-)

diff --git a/src/SaveLocation.cpp b/src/SaveLocation.cpp
index 7d6135a..bb87047 100644
--- a/src/SaveLocation.cpp
+++ b/src/SaveLocation.cpp
@@ -35,8 +35,8 @@ SaveLocation::SaveLocation(QWidget *parent)
 {
     setupUi(this);
 
+    u_urlRequester->setMode(KFile::Directory);
     connect(u_urlRequester, &KUrlRequester::textChanged, this, &SaveLocation::updateGui);
-    connect(u_urlRequester, &KUrlRequester::urlSelected,  this, &SaveLocation::getDir);
     connect(u_imgPrefix, &QLineEdit::textChanged, this, &SaveLocation::updateGui);
     connect(u_imgFormat, static_cast<void (QComboBox::*)(const QString &)>(&QComboBox::activated), this, &SaveLocation::updateGui);
     connect(u_numStartFrom, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &SaveLocation::updateGui);
@@ -52,7 +52,8 @@ void SaveLocation::updateGui()
         u_numStartFrom->setValue(1); // Reset the counter whenever the directory or the prefix is changed
     }
     const QString name = QString::fromLatin1("%1%2.%3").arg(u_imgPrefix->text()).arg(u_numStartFrom->value(), 4, 10, QLatin1Char('0')).arg(u_imgFormat->currentText());
-    u_resultValue->setText(QUrl(u_urlRequester->url().resolved(QUrl(name))).toString(QUrl::PreferLocalFile | QUrl::NormalizePathSegments));
+    QString dir = QDir::cleanPath(u_urlRequester->url().toString()).append(QLatin1Char('/')); //make sure whole value is processed as path to directory
+    u_resultValue->setText(QUrl(dir).resolved(QUrl(name)).toString(QUrl::PreferLocalFile | QUrl::NormalizePathSegments));
 }
 
 void SaveLocation::getDir(void)
diff --git a/src/skanlite.cpp b/src/skanlite.cpp
index 7a671a2..1817425 100644
--- a/src/skanlite.cpp
+++ b/src/skanlite.cpp
@@ -368,7 +368,7 @@ void Skanlite::saveImage()
         m_firstImage = false;
     }
 
-    QString dir = m_saveLocation->u_urlRequester->url().url();
+    QString dir = QDir::cleanPath(m_saveLocation->u_urlRequester->url().url()).append(QLatin1Char('/')); //make sure whole value is processed as path to directory
     QString prefix = m_saveLocation->u_imgPrefix->text();
     QString imgFormat = m_saveLocation->u_imgFormat->currentText().toLower();
     int fileNumber = m_saveLocation->u_numStartFrom->value();
@@ -557,8 +557,6 @@ void Skanlite::saveImage()
 
 void Skanlite::getDir(void)
 {
-    // FIXME KF5 / WAIT: this is not working yet due to a bug in frameworkintegration:
-    // see commit: 2c1ee08a21a1f16f9c2523718224598de8fc0d4f for kf5/src/frameworks/frameworkintegration/tests/qfiledialogtest.cpp
     QString dir = QFileDialog::getExistingDirectory(m_settingsDialog, QString(), m_settingsUi.saveDirLEdit->text());
     if (!dir.isEmpty()) {
         m_settingsUi.saveDirLEdit->setText(dir);
-- 
cgit v0.11.2

