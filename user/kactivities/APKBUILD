# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities
pkgver=5.43.0
pkgrel=1
pkgdesc="Runtime and library to organize work into separate activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
license="GPL-2.0 LGPL-2.1"
depends="kactivitymanagerd"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev boost-dev kconfig-dev
	kcoreaddons-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"
builddir="$srcdir/kactivities-$pkgver/build"

prepare() {
	mkdir -p "$builddir"
	default_prepare
}

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="121a3704558bdc8fc4e71519face18593800a549b2cffff1919ccb0255a29d24075523e96ca3bdf7c999b0dca1dd8b30609b3db7e7538c94f550bb7d69f17c19  kactivities-5.43.0.tar.xz"
