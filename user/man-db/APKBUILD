# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=man-db
pkgver=2.7.6.1
pkgrel=2
pkgdesc="The man command and related utilities for examining on-line help files"
url="http://www.nongnu.org/man-db/"
arch="all"
license="GPL-2+"
depends="groff less"
makedepends="db-dev gettext-dev libpipeline-dev zlib-dev"
subpackages="$pkgname-lang $pkgname-doc"
options="!check"  # requires //IGNORE in iconv
source="http://download.savannah.nongnu.org/releases/man-db/man-db-$pkgver.tar.xz
	man-db.trigger
	man-db-2.7.5-iconv.patch"
triggers="man-db.trigger=/usr/share/man"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-setuid \
		--with-sections="1 1p 1x 2 2x 3 3p 3x 4 4x 5 5x 6 6x 7 7x 8 8x 9 0p tcl n l p o" \
		--enable-nls \
		--with-db=db
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	rm "${pkgdir}"/usr/lib/charset.alias
	rm -r "${pkgdir}"/usr/lib/tmpfiles.d  # systemd
}

sha512sums="623c5e7f8b7c289908b2c926f8777293b8d39aeceef0d2509d701a8b0bfa81408650f655c8608318221786c751a79ee91124b07993de5298cd7fa6d8bb737301  man-db-2.7.6.1.tar.xz
0d2ab0b42888178ffb83c5dd5eaac8005f047de56af55eb3046291318fd8ed8c4999a4ea0148367ea07c0a0490eb8b9bc726a03b46533ef51bec6a5747719b64  man-db.trigger
bdf53b2868eb0652f8ca5bec340736a1923b52921bc1a33bde691f005be937f0a01dd32ff46d04ba956aa9c05b2cf276a03877de6c5fd1d997c4a5b029f330e2  man-db-2.7.5-iconv.patch"
