# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=systemsettings
pkgver=5.12.2
pkgrel=1
pkgdesc="KDE system settings configuration utility"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends="kirigami2"
depends_dev="qt5-qtbase-dev kauth-dev kcmutils-dev kcompletion-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev ki18n-dev kio-dev
	kiconthemes-dev kitemviews-dev kservice-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev kactivities-dev kactivities-stats-dev
	khtml-dev kirigami2-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/systemsettings-$pkgver.tar.xz"
builddir="$srcdir/systemsettings-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1c5bcc72130208208d08880c0e53a647333622033bc825961bae835cea677b93101c7e1c285e97c769cef947331efb41c0736e83bbd715d86111ad2444fefef4  systemsettings-5.12.2.tar.xz"
