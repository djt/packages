# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kxmlgui
pkgver=5.43.0
pkgrel=0
pkgdesc="Framework for creating user interfaces using XML"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Test suite requires OpenGL-accelerated X11 session
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kitemviews-dev kconfig-dev
	kconfigwidgets-dev kiconthemes-dev ktextwidgets-dev kglobalaccel-dev
	attica-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"
builddir="$srcdir/kxmlgui-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ace47a5a01267ba33bc9173058a05cb49cfba7d18a171527df4afb094e437885b697fe5ab02d1609f6e577857f3f27d7d4ac2a25f6ea2dd659291d82fe25aa75  kxmlgui-5.43.0.tar.xz"
