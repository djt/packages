# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kplotting
pkgver=5.43.0
pkgrel=0
pkgdesc="Framework for data plotting functions"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Test requires accelerated X11 display.
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kplotting-$pkgver.tar.xz"
builddir="$srcdir/kplotting-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() { 
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="98b06b2f5242e03b4e67bcbb28d3abb695e4fc21ac37de47384fb872045fd64016ebfc276b0bd48e797e5369a747c2da81fd03519e1b6747e48205c53e69c7cb  kplotting-5.43.0.tar.xz"
