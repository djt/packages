# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=pcmanfm-qt
pkgver=0.12.0
pkgrel=0
pkgdesc="File manager and desktop icon manager for LXQt"
url="http://lxqt.org"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev=""
makedepends="cmake extra-cmake-modules lxqt-build-tools libfm-qt-dev>=0.12.0 liblxqt-dev qt5-qtx11extras-dev qt5-qttools-dev kwindowsystem-dev $depends_dev"
install=""
subpackages="$pkgname-doc"
source="https://github.com/lxde/pcmanfm-qt/releases/download/$pkgver/pcmanfm-qt-$pkgver.tar.xz"
builddir="$srcdir/pcmanfm-qt-$pkgver"
# no tests
options="!check"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="78a75ba6211407c55dbf1f0fc2f307d7444a512b65a732fb4c9c51634f465968443340b999dee8d2aa1d6344bcc1affd80e1b867980d269df69580c102037d95  pcmanfm-qt-0.12.0.tar.xz"
