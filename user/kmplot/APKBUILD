# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmplot
pkgver=17.12.2
pkgrel=0
pkgdesc="Mathematical function plotter"
url="https://www.kde.org/applications/education/kmplot/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kcrash-dev
	kguiaddons-dev ki18n-dev kparts-dev kwidgetsaddons-dev kdoctools-dev
	kdelibs4support-dev kdbusaddons-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/kmplot-$pkgver.tar.xz"
builddir="$srcdir/kmplot-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c58acfc3847993e075b6099a694fe484064298ec7c2e7f3671b6c58cc76125817f41e1c7d0096483163a6b3dce8f8b802cbf0a0c882bbbcb2b70e8cafbf4c6ca  kmplot-17.12.2.tar.xz"
