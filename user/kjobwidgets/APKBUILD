# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kjobwidgets
pkgver=5.43.0
pkgrel=0
pkgdesc="Framework providing widgets that show job progress"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kwidgetsaddons-dev
	qt5-qtx11extras-dev"
makedepends="$depends_dev cmake extra-cmake-modules libx11-dev libxext-dev
	libice-dev qt5-qttools-dev doxygen graphviz"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kjobwidgets-$pkgver.tar.xz"
builddir="$srcdir/kjobwidgets-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="41a8a7c9c81f66eb90c196dc05e6159948c30bc1957fc99c477aa56e25f714665d6b5a1af381ebdd1194e4f2a7a68f8975e48d09d98771d67e899ae542d5f6af  kjobwidgets-5.43.0.tar.xz"
