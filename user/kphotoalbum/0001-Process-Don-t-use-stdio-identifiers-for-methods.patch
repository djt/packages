From 6aca702d252e108801b99113149fec1e89434167 Mon Sep 17 00:00:00 2001
From: "A. Wilcox" <AWilcox@Wilcox-Tech.com>
Date: Wed, 14 Mar 2018 23:25:31 -0500
Subject: [PATCH] Process: Don't use stdio identifiers for methods

`stdout` and `stderr` are already used by POSIX <stdio.h>.  This has
already caused problems in the past as the (now removed) comment in
Process.cpp noted with compilation on Mac OS X.

This also causes a problem compiling on the musl libc, which
specifically uses the preprocessor to avoid redefinition and changing of
stdin, stdout, and stderr.

This patch changes the names to be stdErr and stdOut, which are
camel-cased like Qt methods typically are, and do not conflict with
identifiers defined in <stdio.h>.
---
 ImageManager/VideoLengthExtractor.cpp | 12 ++++++------
 Utilities/Process.cpp                 | 12 ++----------
 Utilities/Process.h                   |  4 ++--
 3 files changed, 10 insertions(+), 18 deletions(-)

diff --git a/ImageManager/VideoLengthExtractor.cpp b/ImageManager/VideoLengthExtractor.cpp
index e751655a..9b3e756c 100644
--- a/ImageManager/VideoLengthExtractor.cpp
+++ b/ImageManager/VideoLengthExtractor.cpp
@@ -72,18 +72,18 @@ void ImageManager::VideoLengthExtractor::extract(const DB::FileName &fileName)
 
 void ImageManager::VideoLengthExtractor::processEnded()
 {
-    if ( !m_process->stderr().isEmpty() )
-        qCDebug(ImageManagerLog) << m_process->stderr();
+    if ( !m_process->stdErr().isEmpty() )
+        qCDebug(ImageManagerLog) << m_process->stdErr();
 
     QString lenStr;
     if (MainWindow::FeatureDialog::ffmpegBinary().isEmpty())
     {
-        QStringList list = m_process->stdout().split(QChar::fromLatin1('\n'));
+        QStringList list = m_process->stdOut().split(QChar::fromLatin1('\n'));
         list = list.filter(STR("ID_LENGTH="));
         if ( list.count() == 0 ) {
             qCWarning(ImageManagerLog) << "Unable to find ID_LENGTH in output from MPlayer for file " << m_fileName.absolute() << "\n"
                        << "Output was:\n"
-                       << m_process->stdout();
+                       << m_process->stdOut();
             emit unableToDetermineLength();
             return;
         }
@@ -99,12 +99,12 @@ void ImageManager::VideoLengthExtractor::processEnded()
 
         lenStr = regexp.cap(1);
     } else {
-        QStringList list = m_process->stdout().split(QChar::fromLatin1('\n'));
+        QStringList list = m_process->stdOut().split(QChar::fromLatin1('\n'));
         // ffprobe -v 0 just prints one line, except if panicking
         if ( list.count() < 1 ) {
             qCWarning(ImageManagerLog) << "Unable to parse video length from ffprobe output!"
                        << "Output was:\n"
-                       << m_process->stdout();
+                       << m_process->stdOut();
             emit unableToDetermineLength();
             return;
         }
diff --git a/Utilities/Process.cpp b/Utilities/Process.cpp
index d31699cc..49be0004 100644
--- a/Utilities/Process.cpp
+++ b/Utilities/Process.cpp
@@ -17,14 +17,6 @@
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
-// OS/X defines stdout and stderr as macros, which interfere with our code here:
-#if defined(stdout)
-#undef stdout
-#endif
-#if defined(stderr)
-#undef stderr
-#endif
-
 #include <QTextStream>
 
 #include "Process.h"
@@ -41,12 +33,12 @@ Utilities::Process::Process(QObject *parent) :
     connect( this, SIGNAL(readyReadStandardOutput()), this, SLOT(readStandardOutput()));
 }
 
-QString Utilities::Process::stdout() const
+QString Utilities::Process::stdOut() const
 {
     return m_stdout;
 }
 
-QString Utilities::Process::stderr() const
+QString Utilities::Process::stdErr() const
 {
     return m_stderr;
 }
diff --git a/Utilities/Process.h b/Utilities/Process.h
index b936e684..6280166d 100644
--- a/Utilities/Process.h
+++ b/Utilities/Process.h
@@ -30,8 +30,8 @@ class Process : public QProcess
     Q_OBJECT
 public:
     explicit Process(QObject *parent = nullptr);
-    QString stdout() const;
-    QString stderr() const;
+    QString stdOut() const;
+    QString stdErr() const;
     
 private slots:
     void readStandardError();
-- 
2.15.0

