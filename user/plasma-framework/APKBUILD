# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-framework
pkgver=5.43.0
pkgrel=1
pkgdesc="Frameworks for the KDE Plasma 5 desktop environment"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0 LGPL-2.1"
options="!check"  # Requires accelerated X11 *and* system DBus running.
depends="qt5-qtquickcontrols"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev kactivities-dev
	karchive-dev kconfigwidgets-dev kdbusaddons-dev kdeclarative-dev kio-dev
	kconfig-dev kglobalaccel-dev kguiaddons-dev kiconthemes-dev ki18n-dev
	kservice-dev kwindowsystem-dev knotifications-dev kpackage-dev
	kwayland-dev qt5-qtquickcontrols2-dev kirigami2-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 gettext-dev doxygen
	kdoctools-dev libx11-dev libxcb-dev graphviz qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/plasma-framework-$pkgver.tar.xz"
builddir="$srcdir/plasma-framework-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e86d8ba66edd21712bdaba0d2e86c3a2618b8925115022d3f614d5d0bcc7b9f9c1b33a4d09ca090555c508d316ba7341d85d3f515698982e014d1e5c3d1c7fcc  plasma-framework-5.43.0.tar.xz"
