# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmousetool
pkgver=17.12.2
pkgrel=0
pkgdesc="Tool to assist with clicking the mouse button"
url="https://userbase.kde.org/KMouseTool"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	kdoctools-dev kiconthemes-dev knotifications-dev kxmlgui-dev phonon-dev
	libxtst-dev libxt-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/kmousetool-$pkgver.tar.xz"
builddir="$srcdir/kmousetool-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="260b5fa3af4720e8dd0d073fe27246f55127759aed264567631d722afc28ad6e256904e782ff4c64e6f79da3f28c896bda1a5ab1ba37e5ee02e620b5e88f905d  kmousetool-17.12.2.tar.xz"
