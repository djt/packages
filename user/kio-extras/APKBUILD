# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kio-extras
pkgver=17.12.2
pkgrel=0
pkgdesc="KIO plugins for various data tasks"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Requires fully running KDE system to test.
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev karchive-dev kconfig-dev kio-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	kiconthemes-dev ki18n-dev solid-dev kbookmarks-dev kguiaddons-dev
	kdnssd-dev kpty-dev kactivities-dev phonon-dev libssh2-dev
	libtirpc-dev taglib-dev"
makedepends="$depends_dev cmake extra-cmake-modules shared-mime-info"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/kio-extras-$pkgver.tar.xz"
builddir="$srcdir/kio-extras-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="5918057745fa201460c8a21e48e669313dce28c40c26ee663e53997af0a5109c6f8396c9ab6939527bf3d315ec29d23aa865290c3fec12896e47dd0526d07fb1  kio-extras-17.12.2.tar.xz"
