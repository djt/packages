# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kaffeine
pkgver=2.0.14
pkgrel=0
pkgdesc="Media player with a focus on Digital TV (DVB)"
url="https://www.kde.org/applications/multimedia/kaffeine/"
arch="all"
license="GPL-2.0-only"
depends="vlc"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kcoreaddons-dev ki18n-dev kwidgetsaddons-dev kwindowsystem-dev kio-dev
	kxmlgui-dev solid-dev kdbusaddons-dev vlc-dev libxscrnsaver-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kaffeine/kaffeine-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="03a4ffbfbe7b9c90de546bf7449e8e5f05d5e7023b75b5253297e885c412677a0c4b46c7f60761ef5f48ad3c284591acba6ff0fe4fa7256d5b01ca9a07d7276f  kaffeine-2.0.14.tar.xz"
