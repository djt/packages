# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kemoticons
pkgver=5.43.0
pkgrel=0
pkgdesc="Emoticons to express emotions in KDE"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Test requires accelerated X11 environment.
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kconfig-dev kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"
builddir="$srcdir/kemoticons-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="130011a2158f5d84ba664354900e23eaf8867623eeab0fe018c6b8c60b3fd413d3f5e1ad43a3fe51dbc0153bec3e8c14d88b7f06f82d873c2d7f2237441eb8d8  kemoticons-5.43.0.tar.xz"
