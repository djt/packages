# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kxstitch
pkgver=2.1.0
pkgrel=0
pkgdesc="Cross-stitch pattern editor by KDE"
url="https://userbase.kde.org/KXStitch"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kdoctools-dev kconfig-dev kconfigwidgets-dev kcompletion-dev ki18n-dev
	kio-dev ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev imagemagick-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/kxstitch/$pkgver/kxstitch-$pkgver.tar.xz"
builddir="$srcdir/kxstitch-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c31b8f4c33a86967b02ad2d6c9be84931e607644c8683c41b3160c86fee8714d77a10f770d94ae3905c81261dfdaf13957783a83d2235594656bf2884f944608  kxstitch-2.1.0.tar.xz"
