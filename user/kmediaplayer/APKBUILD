# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmediaplayer
pkgver=5.43.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
url="https://www.kde.org/"
arch="all"
license="X11 LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kparts-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
install=""
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/5.43/portingAids/kmediaplayer-$pkgver.tar.xz"
builddir="$srcdir/kmediaplayer-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# viewtest requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E viewtest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2e4a0ad713da37521e236a4e1e6ad8bb1cec63546dcbb9c114a40b3165228ac447d54b0d774c576bd59ece219af5898fa033464c13b14ac82da7ab7e99fb49e2  kmediaplayer-5.43.0.tar.xz"
