# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=artikulate
pkgver=17.12.2
pkgrel=0
pkgdesc="Pronunciation trainer for languages"
url="https://www.kde.org/applications/education/artikulate/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	kdoctools-dev ki18n-dev kconfig-dev kcrash-dev knewstuff-dev
	kxmlgui-dev karchive-dev qt5-qtmultimedia-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/artikulate-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# TestCourseFiles needs X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E TestCourseFiles
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a5157cb099e32069e30fdee0be3cbc484a5e13b9f968da8bc04526c49db3ba47d07c5deb9a432bcef9154e616aba9ff7911cbb7bc09307e44fbd2149bfd2e9a9  artikulate-17.12.2.tar.xz"
