# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivitymanagerd
pkgver=5.12.2
pkgrel=0
pkgdesc="Service to manage KDE Plasma activities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	boost-dev python3 kconfig-dev kcoreaddons-dev kwindowsystem-dev kio-dev
	kglobalaccel-dev kxmlgui-dev"
install=""
subpackages="$pkgname-lang"
source="http://download.kde.org/stable/plasma/$pkgver/kactivitymanagerd-$pkgver.tar.xz"
builddir="$srcdir/kactivitymanagerd-$pkgver/build"

prepare() {
	mkdir -p "$builddir"
	default_prepare
}

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c8ea26b4948898cd1734bde5a5fe50991c7d61f997fe4539b819e2ef90e3a1c0fb57dec3331106f42616f9ce0485b81bf61a462832a46a35dcbe87050ecc3c08  kactivitymanagerd-5.12.2.tar.xz"
