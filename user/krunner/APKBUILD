# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krunner
pkgver=5.43.0
pkgrel=0
pkgdesc="Parallel query system"
url="https://api.kde.org/frameworks/krunner/html/index.html"
arch="all"
license="LGPL-2.1"
options="!check"  # Test suite requires DBus and X11 session.
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kconfig-dev kcoreaddons-dev
	ki18n-dev kio-dev plasma-framework-dev threadweaver-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 gettext-dev doxygen
	graphviz qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/krunner-$pkgver.tar.xz"
builddir="$srcdir/krunner-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c6ec92e0dbb8eb2d583af1d763cea6df1231851367148a94d8eae6aa88c130798cb8f3f2fb65946c55a51253c91e25fee539d7720a1f7125f78d2d5aabf4b05c  krunner-5.43.0.tar.xz"
