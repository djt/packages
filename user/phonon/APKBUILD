# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=phonon
pkgver=4.10.0
pkgrel=0
pkgdesc="Qt library for playing multimedia files"
url="https://phonon.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
makedepends="cmake extra-cmake-modules pulseaudio-dev
	qt5-qtbase-dev qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-designer"
source="http://download.kde.org/stable/phonon/$pkgver/phonon-$pkgver.tar.xz"
builddir="$srcdir/phonon-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT=TRUE \
		-DWITH_GLIB2=TRUE \
		-DWITH_PulseAudio=TRUE \
		-DPHONON_BUILD_PHONON4QT5=TRUE \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

designer() {
	pkgdesc="$pkgdesc (Qt Designer plugin)"
	install_if="$pkgname=$pkgver-$pkgrel qt5-qttools"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/
	mv "$pkgdir"/usr/lib/qt5/plugins/designer \
		"$subpkgdir"/usr/lib/qt5/plugins/
	rmdir "$pkgdir"/usr/lib/qt5/plugins || true # Never mind
}

sha512sums="6074c7c33edcdfeed8a6199024f1faedebe09652cf5d01075f2d97146b54c4269924b69ca8298e2341c0cf6ca4d18f930a59a937697f6f282b6de8e976d097c4  phonon-4.10.0.tar.xz"
