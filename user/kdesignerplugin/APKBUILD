# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesignerplugin
pkgver=5.43.0
pkgrel=0
pkgdesc="Qt Designer plugin for KDE widgets"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Test requires accelerated X11 display.
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kconfig-dev kcompletion-dev
	kconfigwidgets-dev kiconthemes-dev kio-dev kitemviews-dev
	ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev sonnet-dev
	kplotting-dev kdesignerplugin"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kdesignerplugin-$pkgver.tar.xz"
builddir="$srcdir/kdesignerplugin-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="38422393c7a061fada3054fd511bafa3c2d529fe819c86854cbf48db1ea92ec2496fa3df947a985f029d5d3110fae33c89d60b853d59a601f6f172f5b15c0684  kdesignerplugin-5.43.0.tar.xz"
