# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kitemviews
pkgver=5.43.0
pkgrel=0
pkgdesc="Framework for displaying collections of items"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires X11
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kitemviews-$pkgver.tar.xz"
builddir="$srcdir/kitemviews-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e54af77d29d1aad0a1e0d842d82a23397f98bc2c146d74c6f1609af66099b0b7413b7d33ae1749036e90db54b1f2217d534d598ddead00be31934a6973a77a05  kitemviews-5.43.0.tar.xz"
