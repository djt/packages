# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sysvinit
pkgver=2.88
pkgrel=3
pkgdesc="System V-style init programs"
url="https://savannah.nongnu.org/projects/sysvinit"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="linux-headers"
install="sysvinit.post-upgrade"
options="!check"
provides="/sbin/init"
subpackages="$pkgname-doc"
source="http://download.savannah.nongnu.org/releases/sysvinit/sysvinit-${pkgver}dsf.tar.bz2
	inittab-2.88
	sysvinit-2.88-posix-header.patch
	"
builddir="$srcdir/sysvinit-${pkgver}dsf"

prepare() {
	cd "$builddir"
	default_prepare

	# util-linux
	sed -i -r \
		-e '/^(USR)?S?BIN/s:\<(last|lastb|mesg)\>::g' \
		-e '/^MAN[18]/s:\<(last|lastb|mesg)[.][18]\>::g' \
		src/Makefile

	# procps
	sed -i -r \
		-e '/\/bin\/pidof/d'\
		-e '/^MAN8/s:\<pidof.8\>::g' \
		src/Makefile
}

build() {
	cd "$builddir"
	export DISTRO="Adélie"
	make -C src
}

package() {
	cd "$builddir"
	make -C src install ROOT="$pkgdir"
	rm "$pkgdir"/usr/bin/lastb || true
	install -D -m644 "$srcdir"/inittab-2.88 "$pkgdir"/etc/inittab
}

sha512sums="0bd8eeb124e84fdfa8e621b05f796804ee69a9076b65f5115826bfa814ac1d5d28d31a5c22ebe77c86a93b2288edf4891adc0afaecc4de656c4ecda8a83807bf  sysvinit-2.88dsf.tar.bz2
3866d377873b44fb7675b9f05e28190b99b7fedddd9463a0bf41de6ff7cad90e0a4273a9908b1f5c77abea85aa867e2f20ce4d466ce97607863cd9b122f8e9b0  inittab-2.88
27dfe089660a291cbcba06d8564bad11f7fd7c96629e72c2b005562689dc7d8bb479c760e980590906e98423b991ae0acd048713d3bc372174d55ed894abeb3f  sysvinit-2.88-posix-header.patch"
