# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalcore
pkgver=17.12.2
pkgrel=0
pkgdesc="Library for managing a calendar of events"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.0"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kdelibs4support-dev libical-dev"
makedepends="$depends_dev cmake extra-cmake-modules bison"
checkdepends="tzdata"
install=""
subpackages="$pkgname-dev"
source="http://download.kde.org/stable/applications/$pkgver/src/kcalcore-$pkgver.tar.xz
	posix-header.patch"
builddir="$srcdir/kcalcore-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(RecursOn-RFC2445_RRULETest39.ics|Compat-Mozilla|Compat-Evolution)'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ed9195e902cbeff2d48a013cb4c7cade458109696476496a4f8a7a340016c46c9e90d707292d16dde9d80b19290c6d4562297b71f81a5bfdea0eefe76889943c  kcalcore-17.12.2.tar.xz
d911c2a03b1db1f9f3b19e7087ae5cbee59ced24ded3b43f2d3286651d13e5f531d171cf7dc6c26906116d4b51a79f2801828956fa1f4e7094fa2f53fe2b888f  posix-header.patch"
