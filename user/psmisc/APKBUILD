# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>

pkgname=psmisc
pkgver=23.0
pkgrel=0
pkgdesc="Miscellaneous utilities that use the proc filesystem"
url="https://gitlab.com/psmisc/psmisc"
arch="all"
license="GPLv2+"
makedepends="ncurses-dev gettext-dev autoconf>=2.69 automake"
checkdepends="dejagnu"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.bz2::https://gitlab.com/$pkgname/$pkgname/repository/archive.tar.bz2?ref=v$pkgver
	dont-underlink-peekfd.patch
	fix-peekfd-on-ppc.patch
	musl_ptregs.patch
	"
options="!strip"
builddir="$srcdir/$pkgname-$pkgver"

prepare() {
	ln -fs $pkgname-v$pkgver-* "$builddir"

	default_prepare

	cd "$builddir"
	sh autogen.sh
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-harden-flags \
		--enable-ipv6 \
		--disable-selinux  # is SELinux on Alpine a thing?
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d4f1d90fb92f83b7583cd82a3c1027de0e53fbee85253d796878878d03a52fc220bd25beaec34c651a1ed547fc13919c5c014a522124308a7421d3f64238a70f  psmisc-23.0.tar.bz2
a68c75eb3c66a9df0d4e574a9439eeed0cd2dc97b3cad3fcd8b945619c2ec238b73bf479d1b55ddd4821471cd8934ee1fbc7871c92de7ef72c3d3f989ab62c9f  dont-underlink-peekfd.patch
b16139606e1ccaebd94b7b7a14f49f530b180f1fd24338008e4c4d48761b57953458c46e57b0f7ec191353514f68126f79c281875c4cd56ed2f1220ace131ce1  fix-peekfd-on-ppc.patch
73dec9791e8cdb85cd3d9ef9be4d16e0fef481c6edc334ed9e954829444b1a92b87f7a3a2e6c1d09ac207bed828f214f5f2a95caa66540a7f2a6c58a6b8f6f2c  musl_ptregs.patch"
