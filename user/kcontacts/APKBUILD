# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcontacts
pkgver=17.12.2
pkgrel=0
pkgdesc="Library for working with contact information"
url="https://www.kde.org"
arch="all"
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev ki18n-dev kconfig-dev kcodecs-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
install=""
subpackages="$pkgname-dev $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/kcontacts-$pkgver.tar.xz"
builddir="$srcdir/kcontacts-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# addresstest requires the library to already be installed.
	# picturetest requires X11 running.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(address|picture)test"
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f6b18421b06d6991c0f99921ccfbe30b8ff79e6912fb1dda2fde06da21600ebcf231549176192ee6ff7f4ca043045a3134389c0b6189c8876a999b4d4d4a2a40  kcontacts-17.12.2.tar.xz"
