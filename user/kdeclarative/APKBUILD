# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdeclarative
pkgver=5.43.0
pkgrel=0
pkgdesc="Frameworks for creating KDE components using QML"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
options="!check"  # Requires accelerated X11 desktop running
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kiconthemes-dev kio-dev
	kpackage-dev libepoxy-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 gettext-dev doxygen
	graphviz qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
builddir="$srcdir/kdeclarative-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="5e9494adee19b40ffefbf006e318ba34302e073e9a22a94db255ebba2cec2e066df380063c7c617af81d0fc3f719753e2c050e6daf792f2f3a9d6c08163b616d  kdeclarative-5.43.0.tar.xz"
