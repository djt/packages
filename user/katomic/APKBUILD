# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=katomic
pkgver=17.12.2
pkgrel=0
pkgdesc="Fun, educational game involving molecular geometry"
url="https://games.kde.org/game.php?game=katomic"
arch="all"
license="GPL-2.0"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev ki18n-dev
	kconfig-dev kcrash-dev kwidgetsaddons-dev kxmlgui-dev knewstuff-dev
	kdoctools-dev kdbusaddons-dev libkdegames-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/katomic-$pkgver.tar.xz"
builddir="$srcdir/katomic-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4d6129fabab44cb9b5eea083e7ba3c406581c3b382fda417aad3988de8321b5a17f5ea8af6fc2437a5eb04f87311b964551e54547edbfb854fd2e19e0658dafd  katomic-17.12.2.tar.xz"
