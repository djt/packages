# A. Wilcox <awilfox@adelielinux.org> (20 Jan 2017)
# Broken on PowerPC.  Fails test suite with segmentation fault.
media-libs/libvpx

# A. Wilcox <awilfox@adelielinux.org> (20 Jan 2017)
# Broken on big endian.  Fails 52 tests.
media-libs/openjpeg
