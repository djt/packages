# A. Wilcox <awilfox@adelielinux.org> (15 Jan 2017)
# Does not work on PowerPC (unsupported arch)
media-sound/pulseaudio webrtc-aec

# A. Wilcox <awilfox@adelielinux.org> (20 Jan 2017)
# media-libs/libvpx is masked for non-functional PowerPC code.
media-video/ffmpeg vpx
media-video/vlc vpx

# A. Wilcox <awilfox@adelielinux.org> (20 Jan 2017)
# media-libs/openjpeg is masked for endian brokenness.
app-text/poppler jpeg2k
