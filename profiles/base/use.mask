# We do not use prefix installations here
prefix
prefix-guest

# We do not, under any circumstances, support alt/BSD installs
elibc_FreeBSD

# We do not, under any circumstances, support glibc
elibc_glibc

# We do not run on Atari computers...
elibc_mintlib

# We likewise do not support the unmaintained uclibc any more
elibc_uclibc

# eesh.
elibc_interix
