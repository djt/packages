# Welcome to Adélie Linux.
# 
# This is the base system profile package list.  It is used by Portage to
# determine @system for *every* architecture.
# 
# Therefore, it must be very lightweight and compatible with every arch.

# Also note that Portage requires * before each package name used for the
# system set.



# This is satisfied by apk-tools *or* Portage++ on Adélie.
*virtual/package-manager

# This is required to use a Unix computer at all ;)
*app-arch/libarchive
*virtual/libc

# Satisfy the mta virtual with the one closest to being POSIX.
*mail-mta/postfix

# Satisfy the logger virtual
*app-admin/syslog-ng

# Satisfy the pkgconfig virtual
*dev-util/pkgconf

#
# Required utilities from 1003.1 S&U Issue 7 (2013 Ed.)
#


# Base

# alias(1)
# cd(1)
# command(1)
# getopts(1)
# hash(1)
# printf(1)
# pwd(1)
# read(1)
# sh(1)
# test(1) - and sys-apps/coreutils
# time(1)
# true(1) - and sys-apps/coreutils
# ulimit(1)
# umask(1)
# unalias(1)
# wait(1)
*app-shells/zsh

# ar(1)
# strings(1)
*sys-devel/binutils

# at(1)
# batch(1)
*sys-process/at

# basename(1)
# cat(1)
# chgrp(1)
# chmod(1)
# chown(1)
# cksum(1)
# comm(1)
# csplit(1)
# cut(1)
# date(1)
# dd(1)
# df(1)
# dirname(1)
# du(1)
# env(1)
# expand(1)
# expr(1)
# false(1)
# fold(1)
# head(1)
# id(1)
# join(1)
# ln(1)
# logname(1)
# ls(1)
# mkdir(1)
# mkfifo(1)
# mv(1)
# nice(1)
# nohup(1)
# od(1)
# paste(1)
# pathchk(1)
# pr(1)
# pwd(1) - and app-shells/zsh
# sleep(1)
# sort(1)
# split(1)
# stty(1)
# tee(1)
# test(1) - and app-shells/zsh
# touch(1)
# tr(1)
# true(1) - and app-shells/zsh
# tsort(1)
# tty(1)
# uname(1)
# unexpand(1)
# uniq(1)
# wc(1)
*sys-apps/coreutils

# cmp(1)
# diff(1)
*sys-apps/diffutils

# crontab(1)
*sys-process/fcron

# ed(1)
*sys-apps/ed

# file(1)
*sys-apps/file

# find(1)
# xargs(1)
*sys-apps/findutils

# gencat(1) -> NOT PROVIDED pending patch to sys-libs/musl

# getconf(1)
# localedef(1)
*sys-apps/shimmy

# grep(1)
*sys-apps/grep

# iconv(1)
*sys-apps/noxcuse[noxcuse_apps_iconv]

# kill(1)
# ps(1)
*sys-process/procps[kill]

# locale(1) -> NOT PROVIDED

# logger(1)
# mesg(1)
# renice(1)
# write(1)
*sys-apps/util-linux[tty-helpers]

# lp(1)
net-print/cups

# m4(1)
*sys-devel/m4

# mailx(1)
*mail-client/nail

# man(1)
*sys-apps/man-db

# newgrp(1)
*virtual/shadow

# patch(1)
*sys-devel/patch

# pax(1)
*app-arch/pax

# sed(1)
*sys-apps/sed

# tabs(1)
# tput(1)
*sys-libs/ncurses:0

# uudecode(1)
# uuencode(1)
*app-arch/sharutils


# User Portability Utilities option

# bc(1)
*sys-devel/bc

# bg(1)
# fc(1)
# fg(1)
# jobs(1)
#*app-shells/zsh (already provided above)

# ex(1)
# vi(1)
*app-editors/vim

# more(1)
#*sys-apps/util-linux (already provided above)

# talk(1)
# NOT PROVIDED (net-misc/netkit-talk)


# X/Open System Interface (XSI) option

# admin(1)
# delta(1)
# get(1)
# prs(1)
# rmdel(1)
# sact(1)
# sccs(1)
# unget(1)
# val(1)
# what(1)
# NOT PROVIDED (sccs)

# cal(1)
# ipcrm(1)
# ipcs(1)
#*sys-apps/util-linux (already provided above)

# cflow(1)
*dev-util/cflow

# compress(1)
# uncompress(1)
*app-arch/ncompress

# fuser(1)
*sys-process/psmisc

# link(1)
# nl(1)
# unlink(1)
# who(1)
#*sys-apps/coreutils (already provided above)

# nm(1)
#*sys-devel/binutils (already provided above)

# type(1)
# ulimit(1)
#*app-shells/zsh (already provided above)

# zcat(1) -> NOT PROVIDED

#
# Utilities required by FHS 3.0
#

# cp(1)
# mknod(1)
# mv(1)
# rm(1)
# rmdir(1)
# sync(1)
#*sys-apps/coreutils (already provided above)

# dmesg(1)
# mount(8)
# umount(8)
#*sys-apps/util-linux (already provided above)

# hostname(1)
# netstat(8)
*sys-apps/net-tools[hostname]

# login(1)
# su(1)
#*virtual/shadow (already provided above)

# ping(8)
*net-misc/iputils
