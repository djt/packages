# awilfox <awilfox@adelielinux.org> (27 Mar 2016)
# libsanitizer does not work on musl.
sys-devel/gcc sanitize

# awilfox <awilfox@adelielinux.org> (22 Apr 2016)
# we don't want Gentoo branding on our LibreOffice packages.
app-office/libreoffice branding

# awilfox <awilfox@adelielinux.org> (23 Apr 2016)
# GDB server is not compatible with musl.
sys-devel/gdb server

# awilfox <awilfox@adelielinux.org> (10 Jul 2016)
# GCC Go requires ucontext, which is deprecated and has been removed from musl.
# libvtv requires execinfo.h, which is not going to be implemented in musl:
#     http://www.openwall.com/lists/musl/2015/04/09/3
sys-devel/gcc go vtv

# awilfox <awilfox@adelielinux.org> (06 Aug 2016)
# WeeChat's spelling requires aspell, which not only doesn't work with musl,
# but is also broken on Gentoo in general and unmaintained upstream.
net-irc/weechat spell

# awilfox <awilfox@adelielinux.org> (03 Dec 2016)
# elfutils maintainers said thread support is broken; testsuite locks on musl.
dev-libs/elfutils threads

# awilfox <awilfox@adelielinux.org> (04 Dec 2016)
# Broken on musl.
media-libs/mesa nptl
x11-base/xorg-server nptl

# awilfox <awilfox@adelielinux.org> (15 Jan 2017)
# Does not support musl.
lxqt-base/lxqt-panel cpuload networkmonitor

# awilfox <awilfox@adelielinux.org> (29 Jan 2017)
# Hopelessly broken on musl.
sys-apps/iproute2 iptables
