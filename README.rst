=====================================
 README for Adélie Linux Package Set
=====================================
:Authors:
  * **A. Wilcox**, primary maintainer
  * **Adélie Linux Developers and Users**, contributions
:Status:
  Production
:Copyright:
  © 2017-2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux package set.  It is used by the
Adélie Linux build system for package building to create the repository used
by Adélie's APK package manager.


Licenses
`````````
As the Adélie Linux project is an open-source Linux distribution, packages
contained in this repository must also be provided under a license recognised
by the OSI_.  A separate repository may be provided in the future for certain
non-free components, such as device driver firmware.

.. _OSI: http://opensource.org/licenses/category


Changes
```````
Any changes to this repository - additions, removal, or version bumps - must
be reviewed before being pushed to the master branch.  There are no exceptions
to this rule.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.




Contents
========

This section contains a high-level view of the contents of this repository.
It does not list every package available; it is merely a guide to help you
find what you need.


``system``: System-level packages
`````````````````````````````````
The ``system`` directory contains special packages used by Adélie for core
system functionality.  This directory is kept separate from ``user`` to
facilitate sharing of user packages with other APK-based distributions.


``user``: User packages
```````````````````````
The ``user`` directory contains packages that a user would typically be
interested in installing.  Desktop applications, server software (also known as
*daemons*), and other useful packages can be found here.


``harmony``: Packages affected by Project Harmony
`````````````````````````````````````````````````
The ``harmony`` directory contains packages that are being discussed with
Alpine Linux for later merging.  These packages may either end up in ``system``
or Alpine's ``main`` depending on the outcome of discussions.  (These packages
are always available in the ``system`` repo when built by Adélie.)




Usage
=====

This section contains usage information for this repository.


As an overlay
`````````````

The ``user`` repository can be added as an overlay to any system running APK,
which at the time of this writing includes Alpine Linux, postmarketOS, and a
few others.  However, please do not add ``system`` to a computer running a
different distribution unless you are fully aware of the concerns surrounding
mixing packages in such a manner.  None of the packages in the ``system``
repository are useful for an Alpine computer, and some packages (such as
``baselayout`` or ``adelie-base``) may in fact damage your Alpine installation
if installed.  Please be careful.

The domain ``distfiles.adelielinux.org`` is a round-robin for all available
Adélie mirrors.  You may add a repository named above to
``/etc/apk/repositories``:

::

  https://distfiles.adelielinux.org/adelie/$version/$repo

Where ``$version`` is the version of Adélie Linux you are running, or
``current`` for automatic upgrades, or ``dev`` for a rolling-release
style distribution (which may be unstable - you have been warned!).

``$repo`` should be replaced with the name of the repository you are wanting
to use, such as ``user``.

Run ``apk update`` to update the package index on your local system.  The
packages will then be available to you.


As a repository
```````````````

The Adélie Linux system is preconfigured to use packages in ``system`` and
``user`` for APK.  No further configuration is required.

