# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
_pkgname=easy-kernel
pkgver=4.14.33
pkgrel=6
pkgname=$_pkgname-$pkgver-mc$pkgrel
pkgdesc="The Linux kernel, packaged for your convenience"
url="https://kernel.org/"
arch="all"
options="!check !dbg !strip !tracedeps"
license="GPL-2.0"
depends=""
makedepends="bc gzip lzop openssl-dev xz"
install=""
provides="easy-kernel=$pkgver-r$pkgrel"
subpackages="$_pkgname-modules-$pkgver-mc$pkgrel:modules
	$_pkgname-src-$pkgver-mc$pkgrel:src"
source="https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.14.tar.xz
	https://mirrormaster.adelielinux.org/source/linux-4.14-mc$pkgrel.patch.xz
	config-ppc64
	config-ppc
	config-x86_64
	config-pmmx"
builddir="$srcdir/linux-4.14"

prepare() {
	default_prepare
	cd "$srcdir"
	cat linux-4.14-mc$pkgrel.patch.xz | unxz -> linux-4.14-mc$pkgrel.patch
	cd "$builddir"
	patch -Np1 <../linux-4.14-mc$pkgrel.patch
	cd "$srcdir"
	cp config-$CARCH linux-4.14/.config
	cp -pr linux-4.14 linux-src
}

build() {
	cd "$builddir"
	make LDFLAGS=""
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir"/boot
	make INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		install modules_install

	if [ -f "$pkgdir"/boot/vmlinuz ]; then
		mv "$pkgdir"/boot/vmlinuz \
			"$pkgdir"/boot/vmlinuz-$pkgver-mc$pkgrel-easy
	fi
	if [ -f "$pkgdir"/boot/vmlinux ]; then
		mv "$pkgdir"/boot/vmlinux \
			"$pkgdir"/boot/vmlinux-$pkgver-mc$pkgrel-easy
	fi

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/easy-$pkgver-mc$pkgrel/kernel.release
}

modules() {
	pkgdesc="Modules / device drivers for easy-kernel"
	provides="easy-kernel-modules=$pkgver-r$pkgrel"
	autodeps=0  # modules should not depend on src just for symlink
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/modules "$subpkgdir"/lib/
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy/build
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy/source
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy/build
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy/source
}

src() {
	pkgdesc="Kernel source code used to build the kernel"
	provides="easy-kernel-src=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/src
	mv "$srcdir"/linux-src "$subpkgdir"/usr/src/linux-$pkgver-mc$pkgrel
}
sha512sums="77e43a02d766c3d73b7e25c4aafb2e931d6b16e870510c22cef0cdb05c3acb7952b8908ebad12b10ef982c6efbe286364b1544586e715cf38390e483927904d8  linux-4.14.tar.xz
578d203d90d0a06fcde363cbac4d0a8b6ef8ed9ca4e1ec25707af9bb6e1673e9a759e9b9909ee72d785d95f8a3589afdd897123c5919d2d0040cf5336f183686  linux-4.14-mc6.patch.xz
b1937f62167a86791be12065b0326f35d30e35b0ea81307e79b29d9502bab2bd793e171247c797703ea27c23c0254fb44e156fa0fffd7a2288eb7b737c5ebf68  config-ppc64
419dd30c2d2592293e7b6889a397784fed84a1f686c0d2d262177be98bcb022a9eab4f77c930866fbc8651fcec0a06a12fb796b85591b28f0b9852904347e44a  config-ppc
b89282bacff4d90337eaff02dc6f8122b91b54e8606c667ba3478036268e21477e0bbc98f68f8b5d1a750c8cf9ae82cf3c7be2344c1ddb9f108518cf17984690  config-x86_64
77fe0ae34512cfe4487f4f03cff4b1ea413f1fb40cb9528af6e670c258b6a268d5741b229b4aeeef70e62602813b47e99f59b111acaf82334be3d860d27b5360  config-pmmx"
