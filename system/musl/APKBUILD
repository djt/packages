# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=musl
pkgver=1.1.19
pkgrel=4
pkgdesc="the musl c library (libc) implementation"
url="http://www.musl-libc.org/"
arch="all"
options="!check"
license="MIT"
depends=""
depends_dev="!uclibc-dev"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-dbg libc6-compat:compat:noarch"
case "$BOOTSTRAP" in
nocc)	pkgname="musl-dev"; subpackages="";;
nolibc) ;;
*)	subpackages="$subpackages $pkgname-utils";;
esac
source="http://www.musl-libc.org/releases/musl-$pkgver.tar.gz
	0001-sysconf-Add-_SC_XOPEN_UUCP.patch
	0002-confstr-Add-_CS_POSIX_V7_THREADS_.patch
	0003-pathconf-add-_PC_TIMESTAMP_RESOLUTION.patch
	0004-stdlib-Move-mkostemp-to-_GNU_SOURCE-_BSD_SOURCE.patch
	0005-stdlib-Ensure-C11-fns-are-only-visible-in-C11.patch
	0006-time-C11-visibility-fixes.patch
	0007-abort-raise-SIGABRT-again-if-signal-is-ignored.patch

	2000-pthread-internals-increase-DEFAULT_GUARD_SIZE-to-2-p.patch
	complex-math.patch
	handle-aux-at_base.patch

	ldconfig
	__stack_chk_fail_local.c
	getent.c
	iconv.c
	"

# secfixes:
#   1.1.15-r4:
#     - CVE-2016-8859

builddir="$srcdir"/musl-$pkgver

build() {
	cd "$builddir"

	[ "$BOOTSTRAP" = "nocc" ] && return 0

	# provide minimal libssp_nonshared.a so we don't need libssp from gcc
	${CROSS_COMPILE}gcc $CPPFLAGS $CFLAGS -c "$srcdir"/__stack_chk_fail_local.c -o __stack_chk_fail_local.o
	${CROSS_COMPILE}ar r libssp_nonshared.a __stack_chk_fail_local.o

	if [ "$BOOTSTRAP" != "nolibc" ]; then
		# getconf/getent/iconv
		local i
		for i in getent iconv ; do
			${CROSS_COMPILE}gcc $CPPFLAGS $CFLAGS "$srcdir"/$i.c -o $i
		done
	fi

	# note: not autotools
	LDFLAGS="$LDFLAGS -Wl,-soname,libc.musl-${CARCH}.so.1" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

package() {
	cd "$builddir"

	if [ "$BOOTSTRAP" = "nocc" ]; then
		case "$CARCH" in
		aarch64*)	ARCH="aarch64" ;;
		arm*)		ARCH="arm" ;;
		x86)		ARCH="i386" ;;
		x86_64)		ARCH="x86_64" ;;
		ppc)		ARCH="powerpc" ;;
		ppc64*)		ARCH="powerpc64" ;;
		s390*)		ARCH="s390x" ;;
		mips64*)	ARCH="mips64" ;;
		mips*)		ARCH="mips" ;;
		esac

		make ARCH="$ARCH" prefix=/usr DESTDIR="$pkgdir" install-headers || return 1
	else
		make DESTDIR="$pkgdir" install || return 1

		cp libssp_nonshared.a "$pkgdir"/usr/lib || return 1

		# make LDSO the be the real file, and libc the symlink
		local LDSO=$(make -f Makefile --eval "$(echo -e 'print-ldso:\n\t@echo $$(basename $(LDSO_PATHNAME))')" print-ldso)
		mv -f "$pkgdir"/usr/lib/libc.so "$pkgdir"/lib/"$LDSO" || return 1
		ln -sf "$LDSO" "$pkgdir"/lib/libc.musl-${CARCH}.so.1 || return 1
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/lib/libc.so || return 1
		mkdir -p "$pkgdir"/usr/bin || return 1
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/bin/ldd || return 1
	fi

	# remove libintl.h, currently we don't want by default any NLS
	# and use GNU gettext where needed. the plan is to migrate to
	# musl gettext() later on as fully as possible.
	rm "$pkgdir"/usr/include/libintl.h || return 1
}

utils() {
	depends="!uclibc-utils scanelf"
	replaces="libiconv uclibc-utils"
	license="MIT BSD GPL2+"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/sbin
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/ || return 1

	install -D \
		"$builddir"/getent \
		"$builddir"/iconv \
		"$subpkgdir"/usr/bin

	install -D -m755 "$srcdir"/ldconfig "$subpkgdir"/sbin
}

compat() {
	pkgdesc="compatibility libraries for glibc"

	mkdir -p "$subpkgdir"/lib
	case "$CARCH" in
	armel) _ld="ld-linux.so.3" ;;
	armhf | armv7) _ld="ld-linux-armhf.so.3" ;;
	aarch64) _ld="ld-linux-aarch64.so.1" ;;
	x86) _ld="ld-linux.so.2" ;;
	x86_64) _ld="ld-linux-x86-64.so.2"
		# go precompiled binary uses /lib64/ld-linux-x86-64.so.2
		# so we add a symlink
		ln -s lib "$subpkgdir"/lib64
		;;
	mips* | s390*) _ld="ld.so.1" ;;
	ppc64le) _ld="ld64.so.2" ;;
	esac
	ln -sf "/lib/libc.musl-${CARCH}.so.1" "$subpkgdir/lib/$_ld"

	for i in libc.so.6 libcrypt.so.1 libm.so.6 libpthread.so.0 librt.so.1 libutil.so.1; do
		ln -sf "/lib/libc.musl-${CARCH}.so.1" "$subpkgdir/lib/$i"
	done
}

sha512sums="abee52d53af4b3c14c9088866c911a24d2b6ef67dc494f38a7a09dfe77250026f77528c24c52469c89cffa8ced2f0fa95badbdcf8d4460c90faba47e3927bcc5  musl-1.1.19.tar.gz
801e0d8adf1ca3bec1c35ce4fe319be7ce7776967630ec27fea39c896dd0e26f047cae34d1b2702e730815789cdc6bd4df526e9078bf68294bcef35a94c498b1  0001-sysconf-Add-_SC_XOPEN_UUCP.patch
5b648ebfdff20f56c6b82b19361a0045a59be8dfef08f8c37f44e0f780ced5e7f3c4fcee12bb25b0cee62edf8c939bc60530550b4a8fcc2c3b1f40c1744f6307  0002-confstr-Add-_CS_POSIX_V7_THREADS_.patch
4ddfdc5ca9d2f86b0ac278e70155c5fd64ba5b012423de89f8c7e07be42ad02a0a965e915c6ce4e139345c981f0103bdd0145f7d732508aba7e8bddd42541c66  0003-pathconf-add-_PC_TIMESTAMP_RESOLUTION.patch
fb7ea456de9cd7cf36be8bc9fdd4e142c999758b1cf4ee93720724985f256ae80f4f426ced3ac5f732d1d9f4048993a017469b6755807da0231f08d5e0e8a467  0004-stdlib-Move-mkostemp-to-_GNU_SOURCE-_BSD_SOURCE.patch
e99dab95932c0c03d67da0248d63b71581050efed40c5e58b28e226925f6f436b6e7ddfbf03dc83ef4cabead2c9a6bd55a30e09c7ee250ef3482ac5206eb309b  0005-stdlib-Ensure-C11-fns-are-only-visible-in-C11.patch
d0d0817a4e1d57b74cb442a3bf8d8efe39a23a387275b75cba1b2fa354d8a7dc2fd843b5b67584aac86ec97ea394d602ec013c6e8a65da309915b0a80b4a8f98  0006-time-C11-visibility-fixes.patch
6f5f9b5fb5eba638b0d98c68c280dbcc2bdae58d94d9f6966f5de55f6dd5d7f3cdddd9ca758e8861e30841b80ba616a945fb2b885a31481707d578293b187ff0  0007-abort-raise-SIGABRT-again-if-signal-is-ignored.patch
2c8e1dde1834238097b2ee8a7bfb53471a0d9cff4a5e38b55f048b567deff1cdd47c170d0578a67b1a039f95a6c5fbb8cff369c75b6a3e4d7ed171e8e86ebb8c  2000-pthread-internals-increase-DEFAULT_GUARD_SIZE-to-2-p.patch
8909dc260968770ace8f3ffdc04c6c7d20933ff115b4fa3e512fb7460860a8216c73ca7a7ad54f59cb5988ef011f02bf18aa13cc2287cc64ffdb8db84ef69d47  complex-math.patch
6a7ff16d95b5d1be77e0a0fbb245491817db192176496a57b22ab037637d97a185ea0b0d19da687da66c2a2f5578e4343d230f399d49fe377d8f008410974238  handle-aux-at_base.patch
8d3a2d5315fc56fee7da9abb8b89bb38c6046c33d154c10d168fb35bfde6b0cf9f13042a3bceee34daf091bc409d699223735dcf19f382eeee1f6be34154f26f  ldconfig
062bb49fa54839010acd4af113e20f7263dde1c8a2ca359b5fb2661ef9ed9d84a0f7c3bc10c25dcfa10bb3c5a4874588dff636ac43d5dbb3d748d75400756d0b  __stack_chk_fail_local.c
378d70e65bcc65bb4e1415354cecfa54b0c1146dfb24474b69e418cdbf7ad730472cd09f6f103e1c99ba6c324c9560bccdf287f5889bbc3ef0bdf0e08da47413  getent.c
9d42d66fb1facce2b85dad919be5be819ee290bd26ca2db00982b2f8e055a0196290a008711cbe2b18ec9eee8d2270e3b3a4692c5a1b807013baa5c2b70a2bbf  iconv.c"
