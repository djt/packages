# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=abuild
pkgver=3.1.0
_ver=${pkgver%_git*}
pkgrel=2
pkgdesc="Script to build Alpine Packages"
url="http://git.alpinelinux.org/cgit/abuild/"
arch="all"
license="GPL-2.0"
depends="fakeroot sudo pax-utils openssl apk-tools>=2.0.7-r1 libc-utils
	attr libarchive-tools pkgconf patch lzip"
if [ "$CBUILD" = "$CHOST" ]; then
	depends="$depends curl"
fi
makedepends_build="pkgconfig"
makedepends_host="openssl-dev zlib-dev"
makedepends="$makedepends_host $makedepends_build"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="apkbuild-cpan:cpan:noarch apkbuild-gem-resolver:gems:noarch
             abuild-rootbld:_rootbld:noarch"
options="suid !check"
pkggroups="abuild"
source="http://dev.alpinelinux.org/archive/abuild/abuild-$_ver.tar.xz
	posix-abuild.patch
	newapkbuild-cmake.patch
	adelie-customisations.patch
	0001-abuild-add-env-option-to-require-tests.patch
	"
builddir="$srcdir/$pkgname-$_ver"

prepare() {
	default_prepare

	cd "$builddir"
	sed -i -e "/^CHOST=/s/=.*/=$CHOST/" abuild.conf
}

build() {
	cd "$builddir"
	make VERSION="$pkgver-r$pkgrel"
}

package() {
	cd "$builddir"

	make install VERSION="$pkgver-r$pkgrel" DESTDIR="$pkgdir"

	install -m 644 abuild.conf "$pkgdir"/etc/abuild.conf
	install -d -m 775 -g abuild "$pkgdir"/var/cache/distfiles
}

cpan() {
	pkgdesc="Script to generate perl APKBUILD from CPAN"
	depends="perl perl-libwww perl-json perl-module-build-tiny"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/apkbuild-cpan "$subpkgdir"/usr/bin/
}

gems() {
	pkgdesc="APKBUILD dependency resolver for RubyGems"
	depends="ruby ruby-augeas"

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/apkbuild-gem-resolver "$subpkgdir"/usr/bin/
}

_rootbld() {
	pkgdesc="Build packages in chroot"
	depends="abuild bubblewrap gettext git"
	mkdir -p "$subpkgdir"
}

sha512sums="bb9093d67942e3a63e4e053692c0bca30940cae05955518206cd9f7029211a188b7f442456ae126e61cbdca224eddb31e967d5cf0637e16893163cc963871a52  abuild-3.1.0.tar.xz
86194084e95cdb42d4b1bc3d4ff2b8144125aa4e7f74f8c97023c55f2304d27d8c377baf5075666c9b43323506357b962d83d4a30a3f41cde7b53542889adf2d  posix-abuild.patch
705c393b37c37a364473590190122a43a2962946290a873e6685cd4eb43c4874bfdf7489e7b655f870b332dee38edca5dcf2d3906461001875a55b9e6549b824  newapkbuild-cmake.patch
e27f29b94fe55fe59ab9dc28986cd1755a41221ad6b4457c7c0b268e561f7c6946ea6fcb8b7ace897acfde2e4660ec7150ef43fb0c71c6c20dfd38aa1d062140  adelie-customisations.patch
e02cc44c8ad9dd61c9b80684b8cf5b64477a6fd6221cde9efea2a7594c6e7ce01a51f8bd4b80d72f82f7caf93217979fb0b354c420983891fa93f34c4252a035  0001-abuild-add-env-option-to-require-tests.patch"
